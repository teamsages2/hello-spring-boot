﻿#! /bin/sh

# jvm proxies configuration
export JAVA_OPTS="$JAVA_OPTS -Djava.net.useSystemProxies=false"

# jvm size
export JAVA_OPTS="$JAVA_OPTS -Xms2G"
export JAVA_OPTS="$JAVA_OPTS -Xmx2G"

export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStore=/data/sages2-qsam/conf/keystore/TrustStoreServer.jks"
export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStorePassword=sages2"

#KeyStore
export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.keyStore=/data/sages2-qsam/conf/keystore/keystore_server.jks"
export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.keyStorePassword=sages2"

#Check Revocation
#export JAVA_OPTS="$JAVA_OPTS -Dcom.sun.net.ssl.checkRevocation=true"
export JAVA_OPTS="$JAVA_OPTS -Dcom.sun.security.enableCRLDP=true"

# GC strategy
export JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSParallelRemarkEnabled -XX:+ScavengeBeforeFullGC -XX:+CMSScavengeBeforeRemark"

# Default file encoding
export JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8"

# The hotspot server JVM has specific code-path optimizations which yield an approximate 10% gain over the client version.
export JAVA_OPTS="$JAVA_OPTS -server"

# Check for application specific parameters at startup
if [ -r "$CATALINA_BASE/bin/appenv.sh" ]; then
  . "$CATALINA_BASE/bin/appenv.sh"
fi

echo "Using CATALINA_OPTS:"
for arg in $CATALINA_OPTS
do
    echo ">> " $arg
done
echo ""

echo "Using JAVA_OPTS:"
for arg in $JAVA_OPTS
do
    echo ">> " $arg
done
echo "_______________________________________________"
echo ""